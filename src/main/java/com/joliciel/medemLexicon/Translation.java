package com.joliciel.medemLexicon;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Translation {
  private final Language lang;
  private String text = "";
  private String field = "";
  private String style = "";
  private List<String> variants;

  public Translation(final Language lang) {
    this.lang = lang;
  }

  public Language getLang() {
    return this.lang;
  }

  public String getText() {
    return this.text;
  }

  public String getField() {
    return this.field;
  }

  void setText(final String text) {
    this.text = text;
  }

  void setField(final String field) {
    this.field = field;
  }

  public String getStyle() {
    return this.style;
  }

  void setStyle(final String style) {
    this.style = style;
  }

  public List<String> getVariants() {
    if (this.variants == null) {
      final String text = this.text.replaceAll("<.+?/>", "");
      this.variants = Arrays.stream(text.split(","))
          .map(w -> w.trim().replace('’', '\''))
          .filter(w -> w.length() > 0)
          .flatMap(w -> TextUtils.getVariants(w).stream())
          .collect(Collectors.toList());
    }
    return this.variants;
  }

}
