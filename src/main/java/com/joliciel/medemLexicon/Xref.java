package com.joliciel.medemLexicon;

public class Xref {
  private final String type;
  private String value = "";

  public Xref(final String type) {
    this.type = type;
  }

  void setValue(final String value) {
    this.value = value;
  }

  public String getType() {
    return this.type;
  }

  public String getValue() {
    return this.value;
  }
}
