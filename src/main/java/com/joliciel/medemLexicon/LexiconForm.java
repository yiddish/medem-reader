package com.joliciel.medemLexicon;

public class LexiconForm {
  private final FormType type;
  private String value = "";

  public LexiconForm(final FormType type) {
    this.type = type;
  }

  public FormType getType() {
    return this.type;
  }

  public String getValue() {
    return this.value;
  }

  void setValue(final String value) {
    this.value = value;
  }
}
