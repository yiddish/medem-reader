package com.joliciel.medemLexicon;

public class MedemException extends RuntimeException {
  public MedemException(final String message) {
    super(message);
  }

  public MedemException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
