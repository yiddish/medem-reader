package com.joliciel.medemLexicon;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TextUtils {

  private static final String WITH_DELIMITER = "((?<=%1$s)|(?=%1$s))";

  private static final class Part {
    private String text;
    private final boolean optional;

    public Part(final String text, final boolean optional) {
      this.text = text;
      this.optional = optional;
    }

    public String[] getOptions() {
      return this.text.split("/");
    }
  }

  public static Collection<String> getVariants(final String phrase) {
    Collection<String> result = null;
    if (phrase.contains("(") || phrase.contains("/")) {
      final String[] textParts = phrase.split(String.format(WITH_DELIMITER, "[\\(\\) ]"));
      final ArrayList<Part> parts = new ArrayList<>();
      boolean optional = false;
      Part currentPart = null;
      for (final String textPart : textParts) {
        if (textPart.equals("(")) {
          optional = true;
          currentPart = new Part("", optional);
          parts.add(currentPart);
        } else if (textPart.equals(")")) {
          optional = false;
          currentPart = null;
        } else {
          if (optional) {
            currentPart.text += textPart;
          } else {
            parts.add(new Part(textPart, optional));
          }
        }
      }
      Set<String> variants = new TreeSet<>();
      variants.add("");
      for (final Part part : parts) {
        final Set<String> newVariants = new HashSet<>();
        for (final String variant : variants) {
          for (final String option : part.getOptions()) {
            if (!(variant.endsWith(" ") && option.equals(" "))) {
              newVariants.add(variant + option);
            } else {
              newVariants.add(variant);
            }
          }
          if (part.optional) {
            newVariants.add(variant);
          }
        }
        variants = newVariants;
      }
      result = variants;
    } else {
      result = Arrays.asList(phrase);
    }
    return result.stream()
        .map(w -> w.trim())
        .filter(w -> w.length() > 0)
        .collect(Collectors.toSet());
  }


  private static final Pattern punctuation = Pattern.compile("[\\p{Punct}&&[^'\\-\"]]+", Pattern.UNICODE_CHARACTER_CLASS);

  public static String removePunctuation(final String w) {
    return punctuation.matcher(w).replaceAll("");
  }
}
