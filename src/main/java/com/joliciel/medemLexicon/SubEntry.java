package com.joliciel.medemLexicon;

import com.joliciel.jochre.yiddish.lexicons.YiddishTextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class SubEntry {
  private static final Logger LOG = LoggerFactory.getLogger(SubEntry.class);

  private final MainEntry parent;
  private String text = "";
  /**
   * Mark the category by default as a phrase
   */
  private String categoryString = "";
  private String gender = "";
  private String lemma = null;
  private List<Category> categories = null;
  private final Map<FormType, LexiconForm> forms = new HashMap<>();
  private final List<String> notes = new ArrayList<>();
  private final List<Xref> xrefs = new ArrayList<>();
  private final List<Translation> translations = new ArrayList<>();
  private final List<String> complements = new ArrayList<>();

  private final List<Lexicalisation> partLexicaliations = new ArrayList<>();

  public SubEntry(final MainEntry parent) {
    this.parent = parent;
  }

  public String getCategoryString() {
    return this.categoryString;
  }

  private static final String ZAYN = "זײַן";

  public List<Category> getCategories() {
    if (this.categories == null) {
      // split on / except when it is followed by "ak" or "dat" (meaning ak/dat or dat/ak)
      this.categories = Arrays.stream(this.categoryString.split("/(?!(אַק|דאַט))"))
          .filter(key -> key.trim().length() > 0)
          .map(key -> Category.forKey(key.trim()))
          .collect(Collectors.toList());

      if (this.categories.size() == 0 && this.text.length() > 0 && this.text.contains(" ")) {
        this.categories = Arrays.asList(Category.phrase);
      }
    }
    return this.categories;
  }

  public String getGender() {
    return this.gender;
  }

  public String getLemma() {
    return this.lemma;
  }

  void setCategoryString(final String categoryString) {
    this.categoryString = categoryString;
    this.categories = null;
  }

  void setGender(final String gender) {
    this.gender = gender;
  }

  void setLemma(final String lemma) {
    this.lemma = lemma;
  }

  public Map<FormType, LexiconForm> getForms() {
    return this.forms;
  }

  public List<String> getNotes() {
    return this.notes;
  }

  public List<Xref> getXrefs() {
    return this.xrefs;
  }

  public List<Translation> getTranslations() {
    return this.translations;
  }

  /**
   * Verbal complements (case, prepositions, etc.)
   */
  public List<String> getComplements() {
    return this.complements;
  }

  public String getText() {
    return this.text;
  }

  void setText(final String text) {
    this.text = text.trim();
  }

  public MainEntry getParent() {
    return this.parent;
  }

  public String getAnalysisText() {
    if (this.text.length() == 0) {
      return this.parent.getText();
    }
    return this.text
        .replaceAll("\\*", "")
        .replaceAll("<.+>", "");
  }

  public List<Lexicalisation> getPartLexicaliations() {
    return this.partLexicaliations;
  }

  public List<Lexicalisation> getLexicalisations() {
    LOG.debug("Entry: " + this.parent.getText());

    LOG.debug("Subentry: " + this.getText());
    LOG.debug("Category: " + this.getCategoryString());
    if (this.getGender().length() > 0) {
      LOG.debug("Gender: " + this.getGender());
    }
    for (final LexiconForm form : this.getForms().values()) {
      LOG.debug("Form " + form.getType() + ": " + form.getValue());
    }

    final String analysisText = this.getAnalysisText();
    final List<Lexicalisation> variants = new ArrayList<>();
    String originalText = analysisText.replace("'", "");
    originalText = originalText.replace('״', '"');
    originalText = originalText.replace("*", "");
    if (analysisText.endsWith("'") && originalText.length() < 3) {
      originalText += "´";
    }

    final Collection<String> textVariants = TextUtils.getVariants(originalText);

    for (String textVariant : textVariants) {
      // TODO - adjective declination in compound forms
      final String cleanVariant = textVariant.replaceAll("\\|", "");

      String lemma = textVariant.replace("|", "");
      if (this.parent.getSuperscript() > 0) {
        lemma += "_" + this.parent.getSuperscript();
      }

      final String pronunciation = this.parent.getPronunciation();

      if (this.getLemma() != null) {
        lemma = this.getLemma().replaceAll("\\*", "");
        textVariant = lemma;
        if (this.parent.getSuperscript() > 0) {
          lemma += "_" + this.parent.getSuperscript();
        }
      }

      if (textVariant.indexOf(' ') >= 0) {
        // add the parts separately
        final String[] parts = textVariant.replace("|", "").split(" ");
        for (final String part : parts) {
          if (!part.equals("איז") && !part.equals("האָט")) {
            this.partLexicaliations.addAll(this.getForms(part, Category.xrefPart, part, "", "@partOf(" + lemma + ")"));
          }
        }
      }
      for (final Category category : this.getCategories()) {
        switch (category) {
          case verb:
          case verbAuxiliary: {
            this.addVerb(variants, category, textVariant, lemma);
            // handle verbs like "efenen"
            if (textVariant.endsWith("ענ|ען")) {
              final String alternateText = textVariant.substring(0, textVariant.length() - "ענ|ען".length()) + "נ|ען";
              this.addVerb(variants, category, alternateText, lemma);
            }
            break;
          }
          case noun: {
            String morphology = this.getGender() + "s";
            if (this.getGender().equals("pl")) {
              morphology = "p";
            }

            boolean haveDim = false;
            final String attributes = "";
            boolean needsPossessive = false;
            boolean hasPossessive = false;

            final boolean isDerivedForm = (this.getForms().containsKey(FormType.dimsrc) || this.getForms().containsKey(FormType.dim2src));

            if (!isDerivedForm) {
              final String baseText = textVariant.replace("|", "");
              for (final LexiconForm lexiconForm : this.getForms().values()) {
                final String originalForm = lexiconForm.getValue().replace("'", "");
                final String[] forms = originalForm.split("/");
                for (final String form : forms) {
                  String inflectedForm = "";

                  if (form.equals("—") || form.equals("-")) {
                    inflectedForm = baseText;
                  } else if (form.equals("—ות") || form.equals("—אָים") || form.equals("—ת")) {
                    inflectedForm = baseText.substring(0, baseText.length() - 1) + form.substring(1);
                  } else if (form.equals("—ים") || form.equals("—יות")) {
                    final String inflectedBase = YiddishTextUtils.removeEndForm(baseText);
                    if (inflectedBase.endsWith("ה")) {
                      inflectedForm = inflectedBase.substring(0, inflectedBase.length() - 1) + form.substring(1);
                    } else {
                      inflectedForm = inflectedBase + form.substring(1);
                    }
                  } else if (form.equals("-ס")) {
                    inflectedForm = YiddishTextUtils.removeEndForm(baseText) + "ס";
                  } else if (form.startsWith("…")) {
                    final String firstLetter = form.substring(1, 2);
                    inflectedForm = baseText.substring(0, baseText.lastIndexOf(firstLetter)) + form.substring(1);
                  } else if (form.endsWith("־")) {
                    inflectedForm = form + baseText.substring(baseText.indexOf('־') + 1);
                  } else if (form.startsWith("־")) {
                    inflectedForm = baseText.substring(0, baseText.indexOf('־')) + form;
                  } else if (form.equals("ן") || form.equals("ען") || form.equals("עס") || form.equals("ס")
                      || form.equals("ם") || form.equals("ים") || form.equals("ות") || form.equals("ין")
                      || form.equals("טע") || form.equals("קע") || form.equals("עך") || form.equals("שע")
                      || form.equals("יכע") || form.equals("ענע") || form.equals("ע") || form.equals("ער")) {
                    inflectedForm = YiddishTextUtils.removeEndForm(baseText) + form;
                  } else {
                    inflectedForm = form;
                  } // derive the form

                  final FormType formType = lexiconForm.getType();
                  switch (formType) {
                    case pl: {
                      variants.addAll(this.getForms(inflectedForm, category, lemma, this.getGender() + "p",
                          attributes));
                      break;
                    }
                    case fem: {
                      variants.addAll(this.getForms(inflectedForm, category, lemma, "fs", attributes));
                      variants.addAll(this.getForms(YiddishTextUtils.removeEndForm(inflectedForm) + "ס", category, lemma,
                          "fp", attributes));
                      needsPossessive = true;
                      break;
                    }
                    case dim:
                    case dim2: {
                      variants.addAll(
                          this.getForms(inflectedForm, category, lemma, "ns", attributes + ",@dim"));

                      if (inflectedForm.endsWith("ל")) {
                        variants.addAll(this.getForms(inflectedForm + "עך", category, lemma, "np",
                            attributes + ",@dim"));
                        variants.addAll(this.getForms(inflectedForm.substring(0, inflectedForm.length() - 1) + "עלע",
                            category, lemma, "ns", attributes + ",@dim2"));
                        variants.addAll(this.getForms(inflectedForm.substring(0, inflectedForm.length() - 1) + "עלעך",
                            category, lemma, "np", attributes + ",@dim2"));
                      }
                      haveDim = true;
                      break;
                    }
                    case accdat: {
                      variants.addAll(this.getForms(inflectedForm, category, lemma, this.getGender() + "s",
                          attributes + ",@acc,@dat"));
                      break;
                    }
                    case dat: {
                      variants.addAll(this.getForms(inflectedForm, category, lemma, this.getGender() + "s",
                          attributes + ",@dat"));
                      break;
                    }
                    case pos: {
                      variants.addAll(this.getForms(inflectedForm, category, lemma, this.getGender() + "s",
                          attributes + ",@poss"));
                      hasPossessive = true;
                      break;
                    }
                    case indef: {
                      if (inflectedForm.startsWith("אַ ")) {
                        inflectedForm = inflectedForm.substring("אַ ".length());
                      } else if (inflectedForm.startsWith("אַן ")) {
                        inflectedForm = inflectedForm.substring("אַן ".length());
                      }
                      variants.addAll(this.getForms(inflectedForm, category, lemma, "ns", "@indef"));
                      break;
                    }
                    case infl: {
                      variants.addAll(this.getForms(inflectedForm, category, lemma, this.getGender(),
                          attributes + ",@infl"));
                      break;
                    }
                    case supsrc:
                    case compsrc: {
                      // do nothing for now
                      break;
                    }
                    default: {
                      throw new RuntimeException("Unknown form type for " + textVariant + ": " + formType);
                    }
                  }
                } // next form
              } // next form entry

              boolean declinable = false;
              if (textVariant.contains("|") && textVariant.endsWith("ער")) {
                declinable = true;
                final String radical = this.getAdjectiveRadical(textVariant);
                this.addAdjective(variants, Category.noun, radical, lemma, attributes);
              } else {
                variants.addAll(this.getForms(baseText, category, lemma, morphology));
                if (needsPossessive && !hasPossessive) {
                  String possessiveForm = YiddishTextUtils.removeEndForm(baseText);
                  if (possessiveForm.endsWith("ס") || possessiveForm.endsWith("ש") || possessiveForm.endsWith("צ")) {
                    possessiveForm += "עס";
                  } else {
                    possessiveForm += "ס";
                  }
                  variants.addAll(this.getForms(possessiveForm, category, lemma, morphology, "@poss"));
                }
              }

              if (!declinable && !haveDim && textVariant.length() > 2) {
                String root = YiddishTextUtils.removeEndForm(baseText);
                if (root.endsWith("ל")) {
                  root += "כ";
                } else if (root.endsWith("נ") || root.endsWith("מ")) {
                  root += "ד";
                }
                // TODO: are these the right startings for diminutives?
                variants.addAll(this.getForms(root + "ל", category, lemma, "ns", "@dim,@guess"));
                variants.addAll(this.getForms(root + "לעך", category, lemma, "np", "@dim,@guess"));
                if (!root.endsWith("ע") && !root.endsWith("ה")) {
                  variants.addAll(this.getForms(root + "עלע", category, lemma, "ns", "@dim2,@guess"));
                  variants.addAll(this.getForms(root + "עלעך", category, lemma, "np", "@dim2,@guess"));
                } else {
                  variants.addAll(this.getForms(root + "לע", category, lemma, "ns", "@dim2,@guess"));
                }
              }
            } // is this a diminutive of a main form?
            break;
          }
          case pronoun:
          case pronounRelative: {
            // pronoun
            final String baseText = textVariant.replace("|", "");

            boolean isNominative = true;
            if (!this.getForms().containsKey(FormType.nom)) {
              isNominative = false;
            }

            if (isNominative) {
              String morphology = "3s";
              if (textVariant.equals("איך")) {
                morphology = "1s";
              } else if (textVariant.equals("דו")) {
                morphology = "2s";
              } else if (textVariant.equals("ער")) {
                morphology = "3ms";
              } else if (textVariant.equals("זי")) {
                morphology = "3fs";
              } else if (textVariant.equals("עס")) {
                morphology = "3ns";
              } else if (textVariant.equals("מיר")) {
                morphology = "1p";
              } else if (textVariant.equals("איר")) {
                morphology = "2p";
              } else if (textVariant.equals("זײ")) {
                morphology = "3p";
              }

              variants.addAll(this.getForms(baseText, category, lemma, morphology, "@nom"));

              String neuterPredicateIndefiniteForm = null;

              for (final FormType formType : this.getForms().keySet()) {
                final LexiconForm lexiconForm = this.getForms().get(formType);
                String originalForm = lexiconForm.getValue().replace("'", "");
                if (originalForm.startsWith("אַיז/האָט")) {
                  originalForm = originalForm.replaceFirst("/", "");
                }
                final String[] forms = originalForm.split("/");
                for (final String form : forms) {
                  switch (formType) {
                    case ntnomacc: {
                      neuterPredicateIndefiniteForm = form;
                      break;
                    }
                    case dat: {
                      variants.addAll(this.getForms(form, category, lemma, morphology, "@dat"));
                      break;
                    }
                    case acc: {
                      variants.addAll(this.getForms(form, category, lemma, morphology, "@acc"));
                      break;
                    }
                    case accdat: {
                      variants.addAll(this.getForms(form, category, lemma, morphology, "@acc,@dat"));
                      break;
                    }
                    case pl: {
                      if (form.equals("ן") || form.equals("ען") || form.equals("עס") || form.equals("ס")
                          || form.equals("ם") || form.equals("ים") || form.equals("ות") || form.equals("ין")
                          || form.equals("טע") || form.equals("קע") || form.equals("עך") || form.equals("שע")
                          || form.equals("יכע") || form.equals("ענע") || form.equals("ע") || form.equals("ער")) {
                        final String inflectedForm = YiddishTextUtils.removeEndForm(baseText) + form;
                        variants.addAll(this.getForms(inflectedForm, category, lemma, "3p"));
                      } else {
                        variants.addAll(this.getForms(form, category, lemma, "3p"));
                      }
                      break;
                    }
                    case pos: {
                      variants.addAll(this.getForms(form, category, lemma, morphology, "@poss"));
                      break;
                    }
                    default: {
                      throw new RuntimeException("Unknown form type for " + textVariant + ": " + formType);
                    }
                  }
                }
              }

              if (textVariant.contains("|")) {
                // pronoun with regular declinations
                final String radical = SubEntry.getAdjectiveRadical(textVariant);
                this.addAdjective(variants, category, radical, lemma, "", null,
                    neuterPredicateIndefiniteForm);
              } else if (neuterPredicateIndefiniteForm != null) {
                variants.addAll(
                    this.getForms(neuterPredicateIndefiniteForm, category, lemma, "3ns", "@nom,@acc"));
              }
            } else {
              LOG.info("Not nominative");
            } // is this in the nominative form?
            break;
          }
          case adj:
          case adjAttr: {
            // adjective
            final boolean isDerivedForm = this.getForms().containsKey(FormType.compsrc) || this.getForms().containsKey(FormType.supsrc)
                || this.getForms().containsKey(FormType.ptsrc);

            if (!isDerivedForm) {
              String attributes = "";
              if (category == Category.adjAttr) {
                attributes = "@attr";
              }

              String predicateForm = textVariant;
              String neuterPredicateIndefiniteForm = null;

              for (final FormType formType : this.getForms().keySet()) {
                final LexiconForm lexiconForm = this.getForms().get(formType);
                final String formPronunciation = pronunciation;
                final String originalForm = lexiconForm.getValue().replace("'", "");

                final String[] forms = originalForm.split("/");
                for (final String form : forms) {
                  switch (formType) {
                    case comp: {
                      this.addAdjective(variants, Category.adj, form, lemma, attributes + ",@comp");
                      if (!this.getForms().containsKey(FormType.sup)) {
                        if (form.endsWith("ער")) {
                          final String supBase = form.substring(0, form.length() - 2) + "סט";
                          this.addAdjective(variants, Category.adj, supBase, lemma, attributes + ",@sup");
                        }
                      }
                      break;
                    }
                    case sup: {
                      this.addAdjective(variants, Category.adj, form, lemma, attributes + ",@sup");
                      break;
                    }
                    case attr:
                    case infl: {
                      predicateForm = form;
                    }
                    case ntnomacc: {
                      neuterPredicateIndefiniteForm = form;
                    }
                    default: {
                      throw new RuntimeException("Unknown form type for " + textVariant + ": " + formType);
                    }
                  }
                }
              }

              // insert vanishing ayin
              String simpleFormBase = textVariant.replace("עַ", "ע");
              // get rid of declinational ayin
              simpleFormBase = simpleFormBase.replace("·", "");
              final String simpleForm = YiddishTextUtils.getEndForm(SubEntry.getAdjectiveRadical(simpleFormBase));
              final String radical = SubEntry.getAdjectiveRadical(predicateForm);
              this.addAdjective(variants, Category.adj, radical, lemma, attributes, simpleForm,
                  neuterPredicateIndefiniteForm);

              // comparative
              if (!this.getForms().containsKey(FormType.comp)) {
                final String compBase = radical + "ער";
                this.addAdjective(variants, Category.adj, compBase, lemma, attributes + ",@comp");
              }

              // superlative
              if (!this.getForms().containsKey(FormType.comp) || !this.getForms().containsKey(FormType.sup)) {
                String supBase = "";
                if (radical.endsWith("ס")) {
                  supBase = radical + "ט";
                } else {
                  supBase = radical + "סט";
                }
                this.addAdjective(variants, Category.adj, supBase, lemma, attributes + ",@sup");
              }

              // affectionate
              final String affectionateRadical = radical + "ינק";
              this.addAdjective(variants, Category.adj, affectionateRadical, lemma,
                  attributes + ",@affectionate,@guess");

              // derogatory
              String derogatoryRadical = radical;
              if (radical.endsWith("ל")) {
                derogatoryRadical += "עכ";
              } else {
                derogatoryRadical += "לעכ";
              }

              this.addAdjective(variants, Category.adj, derogatoryRadical, lemma,
                  attributes + ",@derogatory,@guess");

              // TODO: where are these endings productive?
              variants.addAll(this.getForms(radical + "קײט", Category.noun, radical + "קײט", "fs", "@guess"));
              if (!radical.endsWith("ע") && !radical.endsWith("ה")) {
                variants.addAll(this.getForms(radical + "הײט", Category.noun, radical + "הײט", "fs", "@guess"));
              }
              // variants.addAll(this.getForms(radical + "ונג",
              // "nc", radical + "ונג", "fs",
              // "@guess"));
              // variants.addAll(this.getForms(radical + "שאַפֿט",
              // "nc", radical + "שאַפֿט", "fs",
              // "@guess"));

              // Add "פֿאַרחלוםט" for "פֿאַרחלומט" (common in historical literature)
              if (radical.contains("חלומ")||radical.contains("יתומ")) {
                final List<Lexicalisation> newVariants = variants.stream().map(
                    lex -> new Lexicalisation(lex.getText().replace("יתומ", "יתום").replace("חלומ", "חלום"),
                        lex.getCategory(),
                        lex.getLemma(),
                        lex.getMorphology(),
                        lex.getAttributes(),
                        lex.getPronunciation())
                ).collect(Collectors.toList());
                variants.addAll(newVariants);
              }
            }
            break;
          }
          case adjPoss: {
            final String radical = SubEntry.getAdjectiveRadical(textVariant);
            variants.addAll(this.getForms(cleanVariant, Category.adj, lemma, "s", "@poss"));
            variants.addAll(this.getForms(radical + "ע", Category.adj, lemma, "p", "@poss"));

            this.addAdjective(variants, Category.pronounPossessive, radical, lemma, "@poss");
            break;
          }
          case adjComp: {
            this.addAdjective(variants, Category.adj, textVariant, lemma, "@comp");
            break;
          }
          case adjPred: {
            variants.addAll(this.getForms(cleanVariant, Category.adj, lemma, "", "@pred"));
            break;
          }
          case adjInvariable: {
            variants.addAll(this.getForms(cleanVariant, Category.adj, lemma, "", "@invariable"));
            break;
          }
          case adjInvariableAttr: {
            variants.addAll(this.getForms(cleanVariant, Category.adj, lemma, "", "@invariable,@attr"));
            break;
          }
          case adjPlural: {
            variants.addAll(this.getForms(cleanVariant, Category.adj, lemma, "p", "@plural"));
            break;
          }
          case adjPredPlural: {
            variants.addAll(this.getForms(cleanVariant, Category.adj, lemma, "p", "@pred,@plural"));
            break;
          }
          case article: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, this.getGender()));
            break;
          }
          case preposition: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case particle: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case interjection: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case conjunction: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case coverb: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case title: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case nounProper: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, this.getGender()));
            break;
          }
          case coll:
          case coll2: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, this.getGender()));
            break;
          }
          case xref: {
            for (final Xref medemLexiconXref : this.getXrefs()) {
              final String xref = medemLexiconXref.getValue().replace("|", "");
              variants.addAll(this.getForms(cleanVariant.replace("|", ""), category, xref, ""));
            }
            break;
          }
          case adv: {
            variants.addAll(this.getForms(cleanVariant.replace("|", ""), category, lemma, ""));
            break;
          }
          case dos: {
            variants.addAll(this.getForms(cleanVariant, Category.noun, lemma, "ns"));
            break;
          }
          case phrase: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case number: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case invariable: {
            variants.addAll(this.getForms(cleanVariant, category, lemma, ""));
            break;
          }
          case verbInfinitive: {
            variants.addAll(this.getForms(cleanVariant.replace("|", ""), Category.verb, lemma, "W"));
            break;
          }
          case prepositionPlusArticle:
          case pronounAccusative:
          case pronounDative:
          case pronounDativeAccusative:
          case pronounAccusativeDative:
          case nounAccusativeDative:
          case nounDative:
          case nounPlural:
          case nounPossessive:
          case verbAuxiliaryPlusInfinitive:
          case none: {
            // do nothing (usually handled elsewhere)
            break;
          }
          default: {
            throw new RuntimeException("Unknown category for text " + textVariant + ": " + category);
          }
        } // switch category
      } // next category
    } // next base text variant

    LOG.debug("# Variants: ");
    for (final Lexicalisation variant : variants) {
      LOG.debug(variant.toString());
    }

    return variants.stream().filter(lex -> lex.getText().length() > 0).collect(Collectors.toList());
  }

  private Collection<Lexicalisation> getForms(final String text, final Category category, final String lemma, final String morphology) {
    return this.getForms(text, category, lemma, morphology, "");
  }

  private Collection<Lexicalisation> getForms(String text, final Category category, String lemma, final String morphology, String attributes) {
    // TODO - encoding: tsvey yudn, pasekh aleph, etc.
    if (text.indexOf('|') >= 0) {
      throw new MedemException("Expected all | to be gone: " + text + " in " + lemma + ", " + category);
    }

    text = text.replace("'", "");
    text = text.replace('´', '\'');
    text = text.trim();
    lemma = lemma.replace("|", "");
    lemma = lemma.replace("·", "");
    lemma = lemma.trim();

    if (attributes.startsWith(",")) {
      attributes = attributes.substring(1);
    }

    final Collection<String> variants = TextUtils.getVariants(text);

    final List<Lexicalisation> forms = new ArrayList<>();
    for (final String variant : variants) {
      final Lexicalisation lex = new Lexicalisation(variant, category, lemma, morphology, attributes, this.parent.getPronunciation());
      forms.add(lex);
    }

    return forms;
  }

  void addVerb(final List<Lexicalisation> variants,
               final Category cat, String text, final String lemma) {
    text = text.replace('·', '|');

    final boolean conjugatedForm = this.getForms().containsKey(FormType.inf);

    String attributes = "";
    if (text.endsWith(" זיך")) {
      attributes += ",@refl";
      text = text.substring(0, text.length() - 4);
    }

    boolean compoundForm = false;
    if (text.contains(" ")) {
      compoundForm = true;
      variants.addAll(this.getForms(text.replace("|", ""), cat, lemma, "W", attributes));
    }

    if (!compoundForm && !conjugatedForm) {
      final String radical = SubEntry.getVerbRadical(text);
      final boolean radicalIsZayn = radical.equals(ZAYN);
      final boolean hasExplicitRadical = text.indexOf('|')>=0;
      if (!hasExplicitRadical && !radicalIsZayn) {
        LOG.warn("Verb without radical: " + text);
        return;
      }

      final String infinitive = text.replace("|", "");
      String thirdPersonPlural = infinitive;

      boolean isImpersonal = false;
      for (final String note : this.getNotes()) {
        if (note.equals("impersonal")) {
          isImpersonal = true;
          attributes += ",@impersonal";
        } else {
          throw new RuntimeException("Unknown note type for " + text + ": " + note);
        }
      }

      String coverb = null;
      if (text.indexOf('|') != text.lastIndexOf('|') || (radicalIsZayn && text.indexOf('|') >= 0)) {
        coverb = text.substring(0, text.indexOf('|'));
      }

      boolean haveIrregularPlurals = false;

      for (final FormType formType : this.getForms().keySet()) {
        final LexiconForm lexiconForm = this.getForms().get(formType);
        String originalForm = lexiconForm.getValue().replace("'", "");
        if (originalForm.startsWith("איז/האָט") || originalForm.startsWith("האָט/איז")) {
          originalForm = originalForm.replaceFirst("/", "");
        }

        final String[] forms = originalForm.split("/");
        for (String form : forms) {
          switch (formType) {
            case ppt: {
              String pastParticiple = text;
              boolean conjugateWithZayn = false;
              boolean conjugateWithHobn = true;
              if (form.startsWith("איז ")) {
                conjugateWithZayn = true;
                conjugateWithHobn = false;
                form = form.substring(4);
              } else if (form.startsWith("האָט ")) {
                conjugateWithZayn = false;
                conjugateWithHobn = true;
                form = form.substring(4);
              } else if (form.startsWith("איזהאָט") || form.startsWith("האָטאיז")) {
                conjugateWithZayn = true;
                conjugateWithHobn = true;
                form = form.substring(7);
              }

              if (form.equals("—")) {
                pastParticiple = infinitive;
              } else if (form.equals("—גע—ט") || form.equals("גע—ט") || form.equals("גע-ט") || form.equals("-גע-ט")) {
                pastParticiple = text.substring(0, text.lastIndexOf('|')) + "ט";
                final int coVerbIndex = pastParticiple.indexOf('|');
                if (coVerbIndex < 0) {
                  pastParticiple = "גע" + pastParticiple;
                } else {
                  pastParticiple = pastParticiple.substring(0, coVerbIndex) + "גע"
                      + pastParticiple.substring(coVerbIndex + 1);
                }
              } else if (form.equals("(גע)-ט") || form.equals("(גע)—ט")) {
                pastParticiple = text.substring(0, text.lastIndexOf('|')) + "ט";
                final String pastParticipleText = pastParticiple.replace("|", "");
                variants.addAll(this.getForms(pastParticipleText, cat, lemma, "K"));
                this.addAdjective(variants, Category.adj, pastParticipleText, pastParticipleText, "@pp");
                final int coVerbIndex = pastParticiple.indexOf('|');
                if (coVerbIndex < 0) {
                  pastParticiple = "גע" + pastParticiple;
                } else {
                  pastParticiple = pastParticiple.substring(0, coVerbIndex) + "גע"
                      + pastParticiple.substring(coVerbIndex + 1);
                }
              } else if (form.startsWith("—") || form.startsWith("-")) {
                pastParticiple = text.substring(0, text.lastIndexOf('|')) + form.substring(1);
                pastParticiple = pastParticiple.replace("|", "");
              } else {
                if (form.indexOf('—') >= 0 || form.indexOf('/') >= 0 || form.indexOf(',') >= 0
                    || form.indexOf('-') >= 0) {
                  throw new RuntimeException("Unexpected ppt form: " + form);
                } else {
                  pastParticiple = form;
                }
              }
              String pastParticipleAttributes = "";
              if (conjugateWithZayn && conjugateWithHobn) {
                pastParticipleAttributes = "@zayn,@hobn";
              } else if (conjugateWithZayn) {
                pastParticipleAttributes = "@zayn";
              }

              variants
                  .addAll(this.getForms(pastParticiple, cat, lemma, "K", pastParticipleAttributes));
              this.addAdjective(variants, Category.adj, YiddishTextUtils.removeEndForm(pastParticiple), pastParticiple,
                  "@pp");
              if (pastParticiple.endsWith("ן") && !pastParticiple.endsWith("ען")) {
                // handle געװאָרפֿן = געװאָרפֿענע
                final String alternatePP = pastParticiple.substring(0, pastParticiple.length() - 1) + "ען";
                this.addAdjective(variants, Category.adj, YiddishTextUtils.removeEndForm(alternatePP), pastParticiple,
                    "@pp");
              }
              break;
            }
            case _13pl: {
              // 1st-3rd person plural form different from infinitive
              variants.addAll(this.getForms(form, cat, lemma, "P13p"));
              thirdPersonPlural = form;

              haveIrregularPlurals = true;
              break;
            }
            case _1sg: {
              variants.addAll(this.getForms(form, cat, lemma, "P1s", attributes));
              break;
            }
            case _2sg: {
              variants.addAll(this.getForms(form, cat, lemma, "P2s", attributes));
              if (form.endsWith("סט")) {
                variants
                    .addAll(this.getForms(form + "ו", cat, lemma, "P2s", attributes + ",@P2s+stu"));
              }
              break;
            }
            case _3sg:
            case _3sgn: {
              variants.addAll(this.getForms(form, cat, lemma, "P3s", attributes));
              break;
            }
            case _1pl: {
              variants.addAll(this.getForms(form, cat, lemma, "P1p", attributes));
              haveIrregularPlurals = true;
              break;
            }
            case _2pl: {
              variants.addAll(this.getForms(form, cat, lemma, "P2p", attributes));
              break;
            }
            case _3pl: {
              variants.addAll(this.getForms(form, cat, lemma, "P3p", attributes));
              thirdPersonPlural = form;
              haveIrregularPlurals = true;
              break;
            }
            case prespt: {
              variants.addAll(this.getForms(form, cat, lemma, "G", attributes));
              this.addAdjective(variants, Category.adj, YiddishTextUtils.removeEndForm(form), form, "");
              break;
            }
            case impsg: {
              variants.addAll(this.getForms(form, cat, lemma, "Y2s", attributes));
              break;
            }
            case imppl: {
              variants.addAll(this.getForms(form, cat, lemma, "Y2p", attributes));
              break;
            }
            default: {
              throw new RuntimeException("Unknown verb form type for " + text + ": " + formType);
            }
          }
        } // next form
      } // next key

      if (haveIrregularPlurals) {
        variants.addAll(this.getForms(infinitive, cat, lemma, "W", attributes));
      } else {
        variants.addAll(this.getForms(infinitive, cat, lemma, "WP13p", attributes));
      }

      final String firstPersonForm = SubEntry.getFirstPersonForm(text);

      if (coverb == null) {
        if (isImpersonal) {
          final boolean needP3s = !this.getForms().containsKey(FormType._3sg) && !this.getForms().containsKey(FormType._3sgn);
          if (needP3s) {
            if (radical.endsWith("ט")) {
              variants.addAll(this.getForms(firstPersonForm, cat, lemma, "P3s", attributes));
            } else {
              variants.addAll(this.getForms(radical + "ט", cat, lemma, "P3s", attributes));
            }
          }

        } else {
          final boolean needP1s = !this.getForms().containsKey(FormType._1sg);
          final boolean needP2s = !this.getForms().containsKey(FormType._2sg);
          final boolean needP3s = !this.getForms().containsKey(FormType._3sg) && !this.getForms().containsKey(FormType._3sgn);
          final boolean needP2p = !this.getForms().containsKey(FormType._2pl);
          final boolean needY2s = !this.getForms().containsKey(FormType.impsg);
          final boolean needY2p = !this.getForms().containsKey(FormType.imppl);

          if (radical.endsWith("ט")) {
            if (needP1s || needP3s || needP2p || needY2s || needY2p) {
              String morphology = "";
              if (needP1s || needP3s || needP2p) {
                morphology += "P";
              }
              if (needP1s) {
                morphology += "1";
              }
              if (needP3s) {
                morphology += "3";
              }
              if (needP1s || needP3s) {
                morphology += "s";
              }
              if (needP2p) {
                morphology += "2p";
              }
              if (needY2s || needY2p) {
                morphology += "Y";
              }
              if (needY2s) {
                morphology += "2s";
              }
              if (needY2p) {
                morphology += "2p";
              }
              variants.addAll(this.getForms(firstPersonForm, cat, lemma, morphology));
            }
            if (needP2s) {
              variants.addAll(this.getForms(radical + "סט", cat, lemma, "P2s"));
              variants
                  .addAll(this.getForms(radical + "סטו", cat, lemma, "P2s", attributes + ",@P2s+stu"));
            }
          } else if (radical.endsWith("ס")) {
            if (needP1s || needP2s || needY2s) {
              String morphology = "";
              if (needP1s || needP2s) {
                morphology += "P";
              }
              if (needP1s) {
                morphology += "1";
              }
              if (needP2s) {
                morphology += "2";
              }
              if (needP1s || needP2s) {
                morphology += "s";
              }
              if (needY2s) {
                morphology += "Y2s";
              }
              variants.addAll(this.getForms(firstPersonForm, cat, lemma, morphology));
            }
            if (needP2s || needP3s || needP2p || needY2p) {
              String morphology = "";
              if (needP2s || needP3s || needP2p) {
                morphology += "P";
              }
              if (needP2s) {
                morphology += "2";
              }
              if (needP3s) {
                morphology += "3";
              }
              if (needP2s || needP3s) {
                morphology += "s";
              }
              if (needP2p) {
                morphology += "2p";
              }
              if (needY2p) {
                morphology += "Y2p";
              }
              variants.addAll(this.getForms(radical + "ט", cat, lemma, morphology));
              if (needP2s) {
                variants
                    .addAll(this.getForms(radical + "טו", cat, lemma, "P2s", attributes + ",@P2s+stu"));
              }
            }
          } else {
            if (needP1s || needY2s) {
              String morphology = "";
              if (needP1s) {
                morphology += "P1s";
              }
              if (needY2s) {
                morphology += "Y2s";
              }
              variants.addAll(this.getForms(firstPersonForm, cat, lemma, morphology));
            }
            if (needP3s || needP2p || needY2p) {
              String morphology = "";
              if (needP3s || needP2p) {
                morphology += "P";
              }
              if (needP3s) {
                morphology += "3s";
              }
              if (needP2p) {
                morphology += "2p";
              }
              if (needY2p) {
                morphology += "Y2p";
              }
              variants.addAll(this.getForms(radical + "ט", cat, lemma, morphology));
            }
            if (needP2s) {
              variants.addAll(this.getForms(radical + "סט", cat, lemma, "P2s"));
              variants
                  .addAll(this.getForms(radical + "סטו", cat, lemma, "P2s", attributes + ",@P2s+stu"));
            }
          }
        } // is it an impersonal verb?
      } // have a coverb? If yes, no conjugated forms required

      if (coverb != null) {
        // with co-verb
        String tsuForm = coverb + "צו" + text.substring(text.indexOf('|') + 1);
        tsuForm = tsuForm.replace("|", "");
        variants.addAll(this.getForms(tsuForm, cat, lemma, "W", "@tsu"));
      }

      if (!this.getForms().containsKey(FormType.prespt)) {
        final String presentParticiple = YiddishTextUtils.removeEndForm(thirdPersonPlural) + "דיק";
        variants.addAll(this.getForms(presentParticiple, cat, lemma, "G"));
        this.addAdjective(variants, Category.adj, presentParticiple, presentParticiple, "@prp");
      }

      if (!radicalIsZayn) {
        if (radical.endsWith("ע") && radical.length()>2) {
          variants.addAll(this.getForms(radical + "ר", Category.noun, radical + "ער", "ms", "@guess"));
          variants.addAll(this.getForms(radical + "רס", Category.noun, radical + "ער", "mp", "@guess"));
        } else {
          variants.addAll(this.getForms(radical + "ער", Category.noun, radical + "ער", "ms", "@guess"));
          variants.addAll(this.getForms(radical + "ערס", Category.noun, radical + "ער", "mp", "@guess"));
        }
      }

      // Add "געֵחלוםט" for "געחלומט" (common in historical literature)
      if (radical.contains("חלומ")||radical.contains("יתומ")) {
        final List<Lexicalisation> newVariants = variants.stream().map(
            lex -> new Lexicalisation(lex.getText().replace("יתומ", "יתום").replace("חלומ", "חלום"),
                lex.getCategory(),
                lex.getLemma(),
                lex.getMorphology(),
                lex.getAttributes(),
                lex.getPronunciation())
        ).collect(Collectors.toList());
        variants.addAll(newVariants);
      }
    } // is this an infinitive?
  }

  void addAdjective(final List<Lexicalisation> variants, final Category category, final String radical,
                    final String lemma,
                    final String attributes) {
    this.addAdjective(variants, category, radical, lemma, attributes, null, null);
  }

  void addAdjective(final List<Lexicalisation> variants, final Category category, final String radical,
                    final String lemma,
                    final String attributes, String simpleForm, String neuterPredicateIndefiniteForm) {
    // adjective morphology
    // N = attributive nominative
    // A = attributive accusative
    // D = attributive dative
    // I = attributive nominative neuter preceded by an indefinite article
    // P = predicative
    // J = predicative neuter preceded by an indefinite article
    // m = masculine (singular)
    // f = feminine (singular)
    // n = neuter (singular)
    // p = plural
    if (simpleForm == null) {
      simpleForm = YiddishTextUtils.getEndForm(radical);
    }
    if (neuterPredicateIndefiniteForm == null) {
      neuterPredicateIndefiniteForm = radical + "ס";
    }

    if (attributes.contains("@attr")) {
      variants.addAll(this.getForms(simpleForm, category, lemma, "In", attributes));
    } else {
      variants.addAll(this.getForms(simpleForm, category, lemma, "InPmfnp", attributes));
      variants.addAll(this.getForms(neuterPredicateIndefiniteForm, category, lemma, "Jn", attributes));
    }

    // if ends with ayin, don't double it
    String noAyinRadical = radical;
    if (radical.endsWith("ע")) {
      noAyinRadical = radical.substring(0, radical.length() - 1);
    }

    variants.addAll(this.getForms(noAyinRadical + "ער", category, lemma, "NmDf", attributes));
    variants.addAll(this.getForms(noAyinRadical + "ע", category, lemma, "NfnpAfnpDp", attributes));
    if (radical.equals("נײַ")) {
      variants.addAll(this.getForms(radical + "עם", category, lemma, "AmDmn", attributes));
    } else if (YiddishTextUtils.endsWithVowel(noAyinRadical)) {
      variants.addAll(this.getForms(noAyinRadical + "ען", category, lemma, "AmDmn", attributes));
    } else if (noAyinRadical.endsWith("נ")) {
      variants.addAll(this.getForms(noAyinRadical + "עם", category, lemma, "AmDmn", attributes));
    } else if (noAyinRadical.endsWith("מ") || noAyinRadical.endsWith("נג")) {
      variants.addAll(this.getForms(noAyinRadical + "ען", category, lemma, "AmDmn", attributes));
      if (!radical.endsWith("ע")) {
        variants.addAll(this.getForms(radical + "ן", category, lemma, "AmDmn", attributes));
      }
    } else {
      variants.addAll(this.getForms(radical + "ן", category, lemma, "AmDmn", attributes));
    }
  }

  public void removeAuxiliaryVerbs() {
    this.categories = this.getCategories().stream().filter(c -> !c.isAuxiliaryVerb()).collect(Collectors.toList());
  }

  static String getVerbRadical(final String lemma) {
    String radical;
    if (lemma.equals(ZAYN)) {
      return ZAYN;
    }
    if (lemma.indexOf('|') >= 0) {
      if (lemma.substring(lemma.indexOf('|')+1).equals(ZAYN)) {
        return ZAYN;
      } else {
        radical = lemma.substring(0, lemma.lastIndexOf('|')).replace("|", "");
      }
    } else {
      if (lemma.endsWith("ן")) {
        radical = lemma.substring(0, lemma.length() - 1);
      } else {
        radical = lemma;
      }
    }
    radical = YiddishTextUtils.removeEndForm(radical);

    return radical;
  }

  static String getAdjectiveRadical(final String lemma) {
    String radical = lemma;
    final int verticalBarIndex = lemma.lastIndexOf('|');
    if (verticalBarIndex >= 0) {
      radical = lemma.substring(0, lemma.lastIndexOf('|')).replace("|", "");
    }
    radical = YiddishTextUtils.removeEndForm(radical);
    radical = radical.replace('·', 'ע');
    radical = radical.replace("עַ", "");
    return radical;
  }

  static String getFirstPersonForm(final String infinitive) {
    String firstPersonForm = infinitive;
    final int verticalBarIndex = infinitive.lastIndexOf('|');
    if (verticalBarIndex >= 0) {
      firstPersonForm = infinitive.substring(0, infinitive.lastIndexOf('|')).replace("|", "");
    }
    firstPersonForm = YiddishTextUtils.getEndForm(firstPersonForm);
    return firstPersonForm;
  }
}
