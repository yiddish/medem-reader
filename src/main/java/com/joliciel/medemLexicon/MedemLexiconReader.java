package com.joliciel.medemLexicon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MedemLexiconReader {
  private static final Logger LOG = LoggerFactory.getLogger(MedemLexiconReader.class);

  private enum Tag {
    list,
    entry_count,
    version,
    schema_version,
    entry,
    noun,
    verb,
    gender,
    subentry,
    xref,
    gloss,
    form,
    note,
    text,
    pronunciation,
    sup,
    field,
    style,
    category,
    gram,
    yembed,
    pron,
    complement,
    lemma,
    comment
  }

  private final List<MainEntry> entries = new ArrayList<>();

  public MedemLexiconReader(final File inFile) throws IOException, XMLStreamException {
    final List<File> files;
    if (inFile.isDirectory()) {
      files = Stream.of(inFile.listFiles())
          .filter(f -> !f.isDirectory())
          .filter(f -> f.getName().endsWith(".xml"))
          .collect(Collectors.toList());

      files.sort(new Comparator<File>() {
        @Override
        public int compare(final File file1, final File file2) {
          return file1.getName().compareTo(file2.getName());
        }
      });
    } else {
      files = Arrays.asList(inFile);
    }

    for (final File file : files) {
      LOG.info("Reading file: " + file.getName());
      MedemLexiconReader.readFile(file, this.entries);
    }
  }

  public List<MainEntry> getEntries() {
    return this.entries;
  }

  private static void readFile(final File file, final List<MainEntry> entries) throws IOException, XMLStreamException {
    final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    final XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));

    MainEntry entry = null;
    SubEntry subEntry = null;
    Xref xref = null;
    Translation translation = null;
    LexiconForm form = null;
    final Stack<Tag> elementStack = new Stack<>();
    while (reader.hasNext()) {
      final XMLEvent event = reader.nextEvent();
      if (event.isStartElement()) {
        final StartElement startElement = event.asStartElement();
        final Tag tag = Tag.valueOf(startElement.getName().getLocalPart().replace('-', '_'));
        elementStack.push(tag);
        switch (elementStack.peek()) {
          case entry:
            entry = new MainEntry();
            entries.add(entry);
            break;
          case noun:
            subEntry.setCategoryString("noun");
            break;
          case verb:
            subEntry.setCategoryString("verb");
            break;
          case gender:
            final Attribute gender = startElement.getAttributeByName(new QName("type"));
            if (gender != null) {
              subEntry.setGender(gender.getValue());
            }
            break;
          case subentry:
            subEntry = new SubEntry(entry);
            entry.getSubEntries().add(subEntry);
            break;
          case xref:
            final Attribute xrefType = startElement.getAttributeByName(new QName("type"));
            if (xrefType != null) {
              final String xrefTypeValue = xrefType.getValue();
              xref = new Xref(xrefTypeValue);
              subEntry.getXrefs().add(xref);
            }
            break;
          case gloss:
            if (subEntry == null) {
              throw new RuntimeException("gloss outside of subentry");
            }
            final Attribute lang = startElement.getAttributeByName(new QName("lang"));
            if (lang != null) {
              final Language language = Language.valueOf(lang.getValue());
              translation = new Translation(language);
              subEntry.getTranslations().add(translation);
            }
            break;
          case form:
            if (subEntry == null) {
              throw new RuntimeException("form outside of subentry");
            }
            final Attribute formTypeAttribute = startElement.getAttributeByName(new QName("type"));
            if (formTypeAttribute != null) {
              String formTypeString = formTypeAttribute.getValue();
              if (Character.isDigit(formTypeString.charAt(0))) {
                formTypeString = "_" + formTypeString;
              }
              final FormType formType = FormType.valueOf(formTypeString);
              form = new LexiconForm(formType);
              subEntry.getForms().put(formType, form);
            } else {
              throw new RuntimeException(String.format("form without type: %s", startElement.toString()));
            }
            break;
          case note:
            if (subEntry == null) {
              throw new RuntimeException("note outside of subentry");
            }
            final Attribute note = startElement.getAttributeByName(new QName("type"));
            if (note != null) {
              subEntry.getNotes().add(note.getValue());
            }
            break;
        }
      } else if (event.isEndElement()) {
        switch (elementStack.peek()) {
          case entry:
            entry.clean();
            break;
          default:
            break;
        }
        elementStack.pop();
      } else if (event.isCharacters()) {
        final Tag grandparent = elementStack.size() >= 3 ? elementStack.get(elementStack.size() - 3) : null;
        final Tag parent = elementStack.size() >= 2 ? elementStack.get(elementStack.size() - 2) : null;
        final Tag container = elementStack.peek();
        switch (container) {
          case text:
            switch (parent) {
              case entry:
                entry.setText(entry.getText() + event.asCharacters().getData().replaceAll("עַ", "ע"));
                LOG.debug("Added entry: " + entry.getText());
                break;
              case subentry:
                subEntry.setText(subEntry.getText() + event.asCharacters().getData().replaceAll("עַ", "ע"));
                LOG.debug("Added subEntry: " + subEntry.getText());
                break;
              default:
                throw new RuntimeException("Unexpected <text> element in " + parent);
            }
            break;
          case pronunciation:
            entry.setPronunciation(event.asCharacters().getData());
            break;
          case xref:
            xref.setValue(event.asCharacters().getData());
            break;
          case lemma:
            subEntry.setLemma(event.asCharacters().getData());
            break;
          case form:
            form.setValue(event.asCharacters().getData());
            break;
          case sup: {
            final String data = event.asCharacters().getData();
            switch (parent) {
              case text:
                entry.setSuperscript(Integer.parseInt(data));
                break;
              case gloss:
              case style:
              case yembed:
                translation.setText(translation.getText() + "<sup>" + data + "</sup>");
                break;
              case field:
                translation.setField(translation.getField() + "<sup>" + data + "</sup>");
                break;
              case xref:
                // do nothing for now
                break;
              case form:
                // do nothing for now
                break;
              default:
                throw new RuntimeException("Unexpected <sup> element in " + parent);
            }

            break;
          }
          case gloss:
            translation.setText(event.asCharacters().getData());
            break;
          case field: {
            Tag myContainer = parent;
            if (myContainer == Tag.field) {
              myContainer = grandparent;
            }
            switch (myContainer) {
              case gloss:
                translation.setField(event.asCharacters().getData());
                break;
              default:
                throw new RuntimeException("Unexpected <field> element in " + myContainer);
            }
            break;
          }
          case style:
          case yembed:
          case pron:
          case comment: {
            Tag myContainer = parent;
            if (myContainer == Tag.style || myContainer == Tag.field) {
              myContainer = grandparent;
            }
            switch (myContainer) {
              case gloss:
                translation.setText(translation.getText() + "<" + container.name() + ">" + event.asCharacters().getData() + "</" + container.name() + ">");
                break;
              case pronunciation:
                entry.setPronunciation(entry.getPronunciation() + "<" + container.name() + ">" + event.asCharacters().getData() + "</" + container.name() + ">");
                break;
              default:
                throw new RuntimeException("Unexpected <" + container + "> element in " + myContainer);
            }
            break;
          }
          case complement:
            subEntry.getComplements().add(event.asCharacters().getData());
            break;
          case gram: {
            final String text = event.asCharacters().getData();
            switch (parent) {
              case complement:
                subEntry.getComplements().add(text);
                break;
              case text: {
                switch (grandparent) {
                  case subentry:
                    subEntry.setText(subEntry.getText() + "<gram>" + text + "</gram>");
                    break;
                  case entry:
                    entry.setText(entry.getText() + "<gram>" + text + "</gram>");
                    break;
                  default:
                    throw new RuntimeException("Unexpected <gram> element in <text> in " + grandparent);
                }

                break;
              }
              default:
                throw new RuntimeException("Unexpected <gram> element in " + parent);
            }
            break;
          }
          case category:
            subEntry.setCategoryString(event.asCharacters().getData());
            break;
        }
      }
    }
  }

  private enum Command {
    forms,
    translations,
    inflected_groups
  }

  public static void main(final String[] args) throws Exception {
    final long startTime = (new Date()).getTime();
    try {
      final Command command = Command.valueOf(args[0]);
      final File inFile = new File(args[1]);

      final MedemLexiconReader reader = new MedemLexiconReader(inFile);

      final File outFile = new File(args[2]);
      outFile.getParentFile().mkdirs();
      outFile.delete();

      final List<MainEntry> entries = reader.getEntries();

      switch (command) {
        case forms: {
          try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile, true), "UTF8"))) {
            for (final MainEntry mainEntry : entries) {
              for (final SubEntry subEntry : mainEntry.getSubEntries()) {
                for (final Lexicalisation lex : subEntry.getLexicalisations()) {
                  writer.write(lex.toString() + "\n");
                  writer.flush();
                }
              }
            }
          }
          break;
        }
        case translations: {
          try (final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile, true), "UTF8"))) {
            for (final MainEntry mainEntry : entries) {
              for (final SubEntry subEntry : mainEntry.getSubEntries()) {
                final List<Lexicalisation> lexicalisations = subEntry.getLexicalisations();
                final Set<String> translations = new TreeSet<>();
                for (final Category category : subEntry.getCategories()) {
                  for (final Lexicalisation lex : lexicalisations) {
                    if (category.equals(lex.getCategory().getGeneralCategory())) {
                      for (final Translation translation : subEntry.getTranslations()) {
                        for (final String frenchLex : translation.getVariants()) {
                          translations.add(TextUtils.removePunctuation(lex.getText()) + "\t" + TextUtils.removePunctuation(frenchLex) + "\n");
                        }
                      }
                    }
                  }
                }
                for (final String translation : translations) {
                  writer.write(translation);
                  writer.flush();
                }
              }
            }
          }
          break;
        }
        case inflected_groups: {
          try (
              final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile, true), "UTF8"));
              final Scanner standardSynonymScanner = new Scanner(MedemLexiconReader.class.getResourceAsStream("/standard-yiddish-synonyms.txt"), StandardCharsets.UTF_8)
          ) {
            while (standardSynonymScanner.hasNextLine()) {
              writer.write(standardSynonymScanner.nextLine() + "\n");
            }
            for (final MainEntry mainEntry : entries) {
              for (final SubEntry subEntry : mainEntry.getSubEntries()) {
                final List<Lexicalisation> lexicalisations = subEntry.getLexicalisations();
                final Set<String> strings = lexicalisations.stream().map(l -> l.getText()).filter(t -> !t.contains(" ")).collect(Collectors.toCollection(TreeSet::new));
                final String lemma;
                if (subEntry.getLemma()!=null) {
                  lemma = subEntry.getLemma();
                } else if (!lexicalisations.isEmpty()) {
                  lemma = lexicalisations.get(0).getLemma();
                } else {
                  lemma = null;
                }
                strings.remove("ס");
                strings.remove("קע");
                strings.remove("קעס");
                if (strings.size()>1) {
                  final String group = String.join(", ", strings);
                  writer.write("# " + lemma + " | " + subEntry.getCategoryString() + "\n");
                  writer.write(group + "\n");
                  writer.flush();
                }
              }
            }
          }
          break;
        }
        default: {
          throw new RuntimeException("Unknown command: " + command);
        }
      }
    } catch (final Exception e) {
      LOG.error("An error occurred", e);
    } finally {
      final long endTime = (new Date()).getTime() - startTime;
      LOG.debug("Total runtime: " + ((double) endTime / 1000) + " seconds");
    }
  }

}
