package com.joliciel.medemLexicon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainEntry {
  private static final Logger LOG = LoggerFactory.getLogger(MainEntry.class);

  private String text = "";
  private String pronunciation = "";
  private int superscript = 0;
  private List<SubEntry> subEntries = new ArrayList<>();

  public String getText() {
    return this.text;
  }

  void setText(final String text) {
    this.text = text.trim();
  }

  public String getPronunciation() {
    return this.pronunciation;
  }

  void setPronunciation(final String pronunciation) {
    this.pronunciation = pronunciation;
  }

  void setSuperscript(final int superscript) {
    this.superscript = superscript;
  }

  public int getSuperscript() {
    return this.superscript;
  }

  public List<SubEntry> getSubEntries() {
    return this.subEntries;
  }

  public void clean() {
    boolean hasVerb = this.subEntries.stream().anyMatch(s -> s.getCategories().contains(Category.verb));
    if (hasVerb) {
      this.subEntries.stream().forEach(s -> s.removeAuxiliaryVerbs());
      this.subEntries = this.subEntries.stream().filter(s -> !s.getCategories().isEmpty()).collect(Collectors.toList());
    }
  }
}
