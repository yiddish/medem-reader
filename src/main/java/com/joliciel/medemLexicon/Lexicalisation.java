package com.joliciel.medemLexicon;

public class Lexicalisation implements Comparable<Lexicalisation> {
  private final String text;
  private final Category category;
  private final String lemma;
  private final String morphology;
  private final String attributes;
  private final String pronunciation;

  public Lexicalisation(final String text, final Category category, final String lemma, final String morphology, final String attributes, final String pronunciation) {
    this.text = text.trim();
    this.category = category;
    this.lemma = lemma;
    this.morphology = morphology;
    this.attributes = attributes;
    this.pronunciation = pronunciation;
  }

  @Override
  public String toString() {
    return this.text.trim() + "\t" + this.category + "\t" + this.lemma.trim() + "\t" + this.morphology + "\t" + this.attributes + "\t"
        + this.pronunciation;
  }

  @Override
  public int compareTo(final Lexicalisation o) {
    if (!this.text.equals(o.text)) {
      return this.text.compareTo(o.text);
    }
    if (!this.category.equals(o.category)) {
      return this.category.compareTo(o.category);
    }
    if (!this.lemma.equals(o.lemma)) {
      return this.lemma.compareTo(o.lemma);
    }
    if (!this.morphology.equals(o.morphology)) {
      return this.morphology.compareTo(o.morphology);
    }
    return this.attributes.compareTo(o.attributes);
  }

  public String getText() {
    return this.text;
  }

  public Category getCategory() {
    return this.category;
  }

  public String getLemma() {
    return this.lemma;
  }

  public String getMorphology() {
    return this.morphology;
  }

  public String getAttributes() {
    return this.attributes;
  }

  public String getPronunciation() {
    return this.pronunciation;
  }
}
