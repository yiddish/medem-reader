package com.joliciel.medemLexicon;

import java.util.HashMap;
import java.util.Map;

public enum Category {
  adj("אַדי"),
  adjAttr("אַדי—עפּי"),
  adjComp("אַדי—קאָמפּ"),
  adjInvariable("אַדי—אינװ"),
  adjInvariableAttr("אַדי—עפּי—אינװ"),
  adjPlural("אַדי—מצ"),
  adjPoss("פּאָס—אַדי"),
  adjPred("אַדי—אַטר"),
  adjPredPlural("אַדי—אַטר—מצ"),
  adv("אַדװ"),
  article("אַרט"),
  coll("קאָל"),
  coll2("(קאָל)"),
  conjunction("קאָנ"),
  coverb("קװ"),
  dos("(דאָס)"),
  infinitive("inf"),
  interjection("אינט"),
  /**
   * Not sure what this means - a mistake in the original lexicon?
   */
  invariable("אינװ"),
  noun("noun"),
  nounAccusativeDative("אַק/דאַט"),
  nounDative("דאַט"),
  nounPlural("מצ"),
  nounPossessive("פּאָס"),
  nounProper("פּנ"),
  number("צװ"),
  particle("פּאַרטיקל"),
  phrase("פֿר"),
  preposition("פּרעפּ"),
  prepositionPlusArticle("פּרעפּ + אַרט"),
  pronoun("פּראָנ"),
  pronounAccusative("פּראָנ—אַק"),
  pronounDative("פּראָנ—דאַט"),
  pronounDativeAccusative("פּראָנ—דאַט/אַק"),
  pronounAccusativeDative("פּראָנ—אַק/דאַט"),
  pronounRelative("פּראָנ—רעל"),
  pronounPossessive("pronounPossessive"),
  title("טיטל"),
  verb("verb"),
  verbAuxiliary("הװ", true),
  verbInfinitive("װ-אינפֿ"),
  verbAuxiliaryPlusInfinitive("הװ + אינפֿ", true),
  xref("xref"),
  xrefPart("xrefPart"),
  none("");

  private final String key;
  private final boolean auxiliaryVerb;

  Category(final String key) {
    this(key, false);
  }

  Category(final String key, final boolean auxiliaryVerb) {
    this.key = key;
    this.auxiliaryVerb = auxiliaryVerb;
  }

  private static Map<String, Category> map;

  public static Category forKey(final String key) {
    if (map == null) {
      map = new HashMap<>();
      for (final Category category : Category.values()) {
        map.put(category.key, category);
      }
    }
    if (map.containsKey(key)) {
      return map.get(key);
    }
    throw new RuntimeException("No Category for key: " + key);
  }

  public Category getGeneralCategory() {
    switch (this) {
      case verb:
      case verbAuxiliary:
      case verbInfinitive:
      case verbAuxiliaryPlusInfinitive:
        return Category.verb;
      case noun:
      case nounAccusativeDative:
      case nounDative:
      case nounPlural:
      case nounPossessive:
      case nounProper:
        return Category.noun;
      case pronoun:
      case pronounAccusative:
      case pronounDative:
      case pronounDativeAccusative:
      case pronounAccusativeDative:
      case pronounPossessive:
      case pronounRelative:
        return Category.pronoun;
      case adj:
      case adjAttr:
      case adjComp:
      case adjInvariable:
      case adjInvariableAttr:
      case adjPlural:
      case adjPoss:
      case adjPred:
      case adjPredPlural:
        return Category.adj;
      default:
        return this;
    }
  }

  public boolean isAuxiliaryVerb() {
    return auxiliaryVerb;
  }
}
