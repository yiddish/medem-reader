package com.joliciel.medemLexicon;

public enum FormType {
  /**
   * Plural
   */
  pl,
  /**
   * Past participle
   */
  ppt,
  /**
   * Feminine
   */
  fem,
  /**
   * Diminutive
   */
  dim,
  /**
   * Double diminutive
   */
  dim2,
  /**
   * Accusative and dative
   */
  accdat,
  /**
   * Dative
   */
  dat,
  /**
   * Possessive
   */
  pos,
  /**
   * Indefinite
   */
  indef,
  /**
   * Infinitive
   */
  inf,
  /**
   * Neuter nominative and accusative
   */
  ntnomacc,
  /**
   * Accusative
   */
  acc,
  /**
   * Nominative
   */
  nom,
  /**
   * Comparative
   */
  comp,
  /**
   * Superlative
   */
  sup,
  /**
   * Attributive
   */
  attr,
  /**
   * Don't know what this means
   */
  mts,
  infl,
  /**
   * 1st and 3rd person plural
   */
  _13pl,
  /**
   * 1st person singular
   */
  _1sg,
  /**
   * 2nd person singular
   */
  _2sg,
  /**
   * 3rd person singular
   */
  _3sg,
  /**
   * 3rd person singular, probably a mistake in the original lexicon.
   */
  _3sgn,
  /**
   * 1st person plural
   */
  _1pl,
  /**
   * 2nd person plural
   */
  _2pl,
  /**
   * 3rd person plural
   */
  _3pl,
  /**
   * Present participle
   */
  prespt,
  /**
   * Imperative singular
   */
  impsg,
  /**
   * Imperative plural
   */
  imppl,
  /**
   * Dative masculine neuter
   */
  datmn,
  supsrc,
  compsrc,
  dimsrc,
  dim2src,
  ptsrc,
}
